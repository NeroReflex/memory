#pragma once

#include <iostream>
#include <cinttypes>
#include <memory>

#if !defined(NDEBUG)
    #include <assert.h>
	#define MEMORY_DBG_ASSERT(x) assert(x)
	#define MEMORY_DBG_ONLY(x) x
#else
	#define MEMORY_DBG_ASSERT(x) /* x */
	#define MEMORY_DBG_ONLY(x) /* x */
#endif